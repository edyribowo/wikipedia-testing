package com.edyribowo.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class CustomFirefoxDriver implements DriverSource {
    @Override
    public WebDriver newDriver() {
        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability("marionette", true);
        FirefoxDriver firefoxDriver= new FirefoxDriver(firefoxOptions);
        return firefoxDriver;
    }

    @Override
    public boolean takesScreenshots() {
        return false;
    }
}
